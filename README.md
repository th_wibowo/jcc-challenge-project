Dibuat Oleh: Tatas Hendro Wibowo, kelas Vue JS Jabar Coding Camp batch#2

Keterangan: Aplikasi bansos-app untuk pendataan bansos untuk target pengguna dengan usia 40-50 tahun. Dari segi layout aplikasi dibuat se-simple mungkin, dengan ukuran font, kolom input data, dan spacing yang cukup besar, serta kontras warna yang cukup baik, sehingga memudahkan pengguna untuk memasukkan data penerima bansos.

Link video demo aplikasi: https://youtu.be/MecaWP9dzo0

Link deploy: https://thw-jcc-challenge-bansos-app.netlify.app/

Dependencies:
- Vue Cli @4.5.12
- Vue Router
- Vuetify
- Vuex @3.6.2
- Vuex Persist
- Vuetify Money
- Vee validate

*)Catatan : di dalam aplikasi terdapat fitur login yang dapat diisi dengan sebarang username & password, dengan tujuan hanya sebagai simulasi.