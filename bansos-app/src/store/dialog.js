export default {
    namespaced: true,
    state: {
      status: false,
    },
    mutations: {//payload adalah variabel untuk menentukan besar increment
      setStatus : (state, status) => {
        state.status = status
      },
    },
    actions: {
      setStatus : ( {commit}, status ) => {
        commit('setStatus', status)
      },
    },
    getters: {
      status: state => state.status,
    }
  
  }