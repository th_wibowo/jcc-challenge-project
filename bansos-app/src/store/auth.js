export default {
    namespaced: true,
    state: {
      guest: true,
      user: [],
    },
    mutations: {
      setUser: (state, payload) => {
        state.guest = payload.guest
        state.user = payload.user
      },
  
    },
    actions: {
        setUser: ({commit}, payload) => {
          commit('setUser', payload)
        },
    },
    getters: {
        guest: state => state.guest,
        user: state => state.user,
    }
  }