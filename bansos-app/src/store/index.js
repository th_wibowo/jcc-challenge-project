import Vue from 'vue'
import Vuex from 'vuex'
import alert from './alert.js'
import auth from './auth.js'
import dialog from './dialog'

import VuexPersist from 'vuex-persist'

const vuexPersist = new VuexPersist({
  key: 'bansosapp',
  storage: localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  modules: {
    alert,
    auth,
    dialog,
  }
})